import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	for (int checkEachWord =0; checkEachWord<4 ; checkEachWord++){
		if (word.charAt(checkEachWord)==c){
			return checkEachWord;
		}
	}
	   return -1;
}
	
	public static void printWork(String word, boolean[] letters) {
		String newWord = "";
		for(int checkEachWord=0; checkEachWord<letters.length;checkEachWord++){
			if(letters[checkEachWord]==true){
				newWord=newWord+word.charAt(checkEachWord);
			}else {
				newWord = newWord+" _ ";
			}
		}
		
		System.out.println("Your result is "+ newWord);
	}
	public static  void runGame(String word){
		Scanner reader = new Scanner (System.in);
		boolean[] letters=new boolean[4];
		int numberOfGuess = 0;
		boolean gotAllWord=false;
		while(numberOfGuess < 6 && !gotAllWord){
			System.out.println("Guess a letter");
			//user guess the word
			char guess = reader.nextLine().charAt(0);
			//tranforme user's answer to upper case
			guess=Character.toUpperCase(guess);
			//check user's answer 
			int result=isLetterInWord(word,guess);
			if(result!=-1){
				letters[result]=true;
			}else{
				numberOfGuess++;
			}
			
			printWork(word, letters);
			//check if the game should finish
			for(int checkEachWord=0;checkEachWord<letters.length;checkEachWord++){
				if(letters[checkEachWord]){
					gotAllWord=true;
				}else{
					gotAllWord=false;
				}
			}
		}
		if (numberOfGuess==6){
		System.out.println("Oops! Better luck next time :)" );
		}
		if(gotAllWord){
		System.out.println("Congrats! You got it :)");
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    runGame(word);	
	}
}